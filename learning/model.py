import mlflow
import os

from sklearn.datasets import load_iris
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler


def train_model():
    mlflow.set_tracking_uri(os.getenv('MLFLOW_TRACKING_URL'))

    with mlflow.start_run(run_name='Iris demo'):
        random_state = 53
        tolerance = 1e-3

        features, target = load_iris(return_X_y=True)

        train_features, test_features, train_target, test_target = train_test_split(
            features,
            target,
            test_size=.25,
            random_state=random_state
        )

        scaler = MinMaxScaler()

        model = LogisticRegression(random_state=random_state, n_jobs=-1, tol=tolerance)

        pipeline = Pipeline(steps=[
            ('scaler', scaler),
            ('linear_regression', model)
        ])

        pipeline.fit(train_features, train_target)

        predicted = pipeline.predict(test_features)

        # Logging parameters
        mlflow.log_param('random_state', random_state)
        mlflow.log_param('tol', tolerance)

        # Logging metrics
        mlflow.log_metric("accuracy", accuracy_score(test_target, predicted))
        mlflow.log_metric("micro precision", precision_score(test_target, predicted, average='micro'))
        mlflow.log_metric("macro precision", precision_score(test_target, predicted, average='macro'))
        mlflow.log_metric("micro recall", recall_score(test_target, predicted, average='micro'))
        mlflow.log_metric("macro recall", recall_score(test_target, predicted, average='macro'))
        mlflow.log_metric("micro f1", f1_score(test_target, predicted, average='micro'))
        mlflow.log_metric("macro f1", f1_score(test_target, predicted, average='macro'))

        # Set model tags
        mlflow.set_tags({'author': 'username@exaple.tld'})
        mlflow.set_tags({'model': 'LogisticRegression'})
        mlflow.set_tags({'dataset': 'Iris'})

        mlflow.sklearn.log_model(pipeline, 'LogisticRegression model demo')


if __name__ == '__main__':
    train_model()
